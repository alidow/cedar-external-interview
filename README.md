# README #

Welcome to the Cedar tech challenge! This one-hour exercise is designed to test your technical ability, intuition, and thought process. 

In this exercise, you'll be given a Django project running in a Docker container. Your challenge is to debug a particularly nasty bug preventing debug output from being displayed correctly. 

The exercise is designed to be difficult, so not everyone will get the "right" answer. This is ok - we're more interested in your thought process, creativity, and the techniques you use to debug the issue.

### Before You Begin ###

* We expect that you're somewhat familiar with Python, bash, and docker.  If you're not, let us know and we can discuss other exercises.

* You'll need to have [docker](https://docs.docker.com/engine/installation/) and [docker-compose](https://docs.docker.com/compose/install/) installed on your machine.

* The exercise will use ports 80 and 8000 on your local machine, so make sure nothing else is running on these ports.

* Clone or download the code to a local folder, then from the command line cd to the directory and run:

```
#!bash

docker-compose up
```

Note that it will take a few minutes for docker to build the images. On my machine, it took about 7 minutes.

* Once the docker containers are up, you should see the following messages in your console:


```
#!bash

cedar-intvw-app-1 | 2016-11-07 23:54:51,420 INFO success: app-uwsgi entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
cedar-intvw-app-1 | 2016-11-07 23:54:51,420 INFO success: nginx-app entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)
```

* In your browser, go to [http://0.0.0.0/webapp/](http://0.0.0.0/webapp/). You should see the message "Hello, world."

* Login to the docker container running the Django app with the following command:
```
#!bash

docker-compose exec cedar-app /bin/bash
```

### The Challenge ###

Debug output in the Django application you've just setup is broken. Your challenge is to fix it.

Normally, a Django application will give you debug information when there's a runtime error. Debug output usually [looks like this](https://www.webforefront.com/static/images/django/customizedjangotemplates_screenshot1.png). However, our application is returning a 502 error instead of the typical debug information.

To replicate the issue, go to [http://0.0.0.0/webapp/oops/](http://0.0.0.0/webapp/oops/). This page loads the "oops" function in views.py, which raises an intentional error so you can see that a 502 response is returned to the browser instead of a normal Django debug message. 

For this challenge, you should spend up to an hour trying to debug the issue. It's okay if you don't solve the problem - it's a tough issue, and we're more interested in the approach you took to solve the problem.

Once you're finished, please send the following to challenge@cedar.com:

* A copy of your bash history. Your history will be conveniently saved in the "bash_history" file in the git project. We trust you not to edit your history.
* If you solved the problem, let us know what was causing the problem and how you solved it.
* If you weren't able to solve the problem, send us a brief (a few bullets) description of what you think is causing the problem and the approach you took to debug the issue, as well as any ideas you didn't have time to try.

### Some (Potentially) Useful Info ###

* We use python 3. If you're running python commands, just remember to use the "python3" command instead of "python".