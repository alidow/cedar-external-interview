#!/bin/bash

# while :; do echo '.'; sleep 10000; done

# sleep for a few seconds to make sure db is up...
sleep 5

cd $CEDAR_HOME
python3 manage.py migrate --noinput
python3 manage.py collectstatic --noinput
/bin/bash $CEDAR_HOME/app/scripts/uwsgi-reload.sh &
./manage.py shell < ./scripts/create-test-objects.py
supervisord -n