while true #run indefinitely
do
inotifywait -r -e modify,attrib,close_write,move,create,delete /usr/local/cedar/app/webapp | grep '\.py' --line-buffered && kill -HUP $(cat /var/run/uwsgi.pid)
done