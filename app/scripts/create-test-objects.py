from webapp.models import *
from datetime import datetime
from dateutil import parser

patient = Patient(
		last_name			= 'Bar',
		first_name			= 'Foo',
		ssn					= '111111111',
		phone_cell			= '1111111',
		phone_home			= '1111111',
		email				= 'foo@bar.com',
		dob					= parser.parse('2000-01-01 00:00AM'),
	)
patient.save()

insurer = Insurer(
		name = 'Aetna',
	)
insurer.save()

provider = Provider(
		organization 	= '....',
		phone_primary	= '1111111',
		email			= 'foo@provider.com',
	)
provider.save()

visit = Visit(
		patient 			= patient,
		provider 			= provider,
		date_of_service 	= parser.parse('2016-11-01 00:00AM UTC'),
	)
visit.save()



