
from django.db import models

class ActivityTimelineEventType(models.Model):
	
	type_id = models.IntegerField()
	name = models.CharField(max_length=255)
	created_on = models.DateTimeField(auto_now_add=True)
	last_modified = models.DateTimeField(auto_now=True)
	deleted = models.BooleanField(default=False)

	EVENT_TYPES = {
		'VISIT'						: 1,
		'INSURANCE_DONE_PROCESSING'	: 2,
		'BILL_SENT_VIA_MAIL'		: 3,
		'BILL_SENT_VIA_EMAIL'		: 4,
		'BILL_SENT_VIA_TEXT_MSG'	: 5,
		'BILL_DUE_DATE'				: 6,
		'COLLECTIONS_DEADLINE'		: 7,
		'FULL_PAYMENT_MADE'			: 8,
		'PARTIAL_PAYMENT_MADE'		: 9,
		'PAYMENT_PLAN_SETUP'		: 10,
		'PAYMENT_PLAN_PAYMENT'		: 11
	}

	def __getattr__(self, name):
		if name in self.EVENT_TYPES:
			return self.EVENT_TYPES[name]
		else:
			return self.__getattribute__(name)

	def __str__(self):
		mapping = {
			self.VISIT						: (lambda: "visit"),
			self.INSURANCE_DONE_PROCESSING	: (lambda: "insurance done processing"),
			self.BILL_SENT_VIA_MAIL			: (lambda: "bill sent via mail"),
			self.BILL_SENT_VIA_EMAIL		: (lambda: "bill sent via email"),
			self.BILL_SENT_VIA_TEXT_MSG		: (lambda: "bill sent via text message"),
			self.BILL_DUE_DATE				: (lambda: "bill due date"),
			self.COLLECTIONS_DEADLINE		: (lambda: "collections deadline"),
			self.FULL_PAYMENT_MADE			: (lambda: "bill fully paid"),
			self.PARTIAL_PAYMENT_MADE		: (lambda: "partial payment made"),
			self.PAYMENT_PLAN_SETUP			: (lambda: "payment plan setup"),
			self.PAYMENT_PLAN_PAYMENT		: (lambda: "payment plan payment made")
		}
		if self.type_id not in mapping:
			return "unknown"
		else:
			return mapping[self.type_id]()

	class Meta:
		db_table = 'activity_timeline_event_type'
