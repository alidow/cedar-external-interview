
from django.db import models

class InvoiceLineItemCategory(models.Model):
    category = models.CharField(max_length=255, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.category

    class Meta:
        db_table = 'invoice_line_item_category'
