
from django.db import models
from django.core.validators import RegexValidator
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save

from .user import User
from .invoice import Invoice

class Patient(models.Model):
    last_name = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    dob = models.DateField()

    ssn_regex = RegexValidator(regex=r'^\d{9}$', message="Value must be a valid social security number.")
    ssn = models.CharField(validators=[ssn_regex], max_length=9, blank=True)

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Value must be a valid phone number.")
    phone_cell = models.CharField(validators=[phone_regex], max_length=16, blank=True)
    phone_home = models.CharField(validators=[phone_regex], max_length=16, blank=True)
    
    email = models.EmailField()
    address = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=2, blank=True)
    zipcode = models.CharField(max_length=10, blank=True)
    country = models.CharField(max_length=3, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    user = models.OneToOneField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)

    def save(self, *args, **kwargs):
        if not self.pk:
            # patient is being created for the first time
            user = User.objects.create_user(email=self.email, last_name=self.last_name, first_name=self.first_name)
            user.save()
            self.user = user

        super(Patient, self).save(*args, **kwargs)

    class Meta:
        db_table = 'patient'

    def update_contact_cell(self, new_cell):
        if new_cell is None:
            return False
        if new_cell != self.phone_cell:
            self.phone_cell = new_cell
            return True
        return False

    def update_contact_home_phone(self, new_phone):
        if new_phone is None:
            return False
        if new_phone != self.phone_home:
            self.phone_home = new_phone
            return True
        return False

    def update_contact_email(self, new_email):
        if new_email is None:
            return False
        if new_email != self.email:
            self.email = new_email
            return True
        return False

    def update_contact_address(self, new_address, new_city, new_state, new_zipcode, new_country):
        if new_address is None:
            return False
        if new_address != self.address:
            self.address    = new_address
            self.city       = new_city
            self.state      = new_state
            self.zipcode    = new_zipcode
            self.country    = new_country
            return True
        return False

    def update_contact_info(self, patient_info):
        if patient_info is None:
            # TODO: log error and throw exception?
            return self
        changed = False
        changed = self.update_contact_cell(patient_info.phone_cell)         or changed
        changed = self.update_contact_home_phone(patient_info.phone_home)   or changed
        changed = self.update_contact_email(patient_info.email)             or changed
        changed = self.update_contact_address(  patient_info.address,
                                                patient_info.city, 
                                                patient_info.state, 
                                                patient_info.zipcode, 
                                                patient_info.country)       or changed
        if changed:
            self.save()
        return self

    __outstanding_invoices = None

    @property
    def outstanding_invoices(self):
        if self.__outstanding_invoices is None:
            self.__outstanding_invoices = Invoice.get_outstanding_invoices_for_patient(patient_id = self.id)
        return self.__outstanding_invoices

    @property
    def latest_outstanding_invoice(self):
        invoices = self.outstanding_invoices
        latest_invoice = None
        for invoice in invoices:
            if (latest_invoice is None) or (invoice.created_on > latest_invoice.created_on):
                latest_invoice = invoice
        return latest_invoice

    @property
    def last_4_ssn(self):
        return self.ssn[-4:]
