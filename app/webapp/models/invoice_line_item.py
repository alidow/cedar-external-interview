
from django.db import models
from .invoice_line_item_category import InvoiceLineItemCategory

class InvoiceLineItem(models.Model):
    patient = models.ForeignKey('Patient', on_delete=models.CASCADE)
    invoice = models.ForeignKey('Invoice', on_delete=models.CASCADE)
    description = models.CharField(max_length=255, blank=True)
    detailed_explanation = models.CharField(max_length=3000, blank=True)
    charge_amount = models.DecimalField(blank=True, null=True, max_digits=12, decimal_places=2)
    invoice_line_item_category = models.ForeignKey(InvoiceLineItemCategory, on_delete=models.CASCADE, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.invoice, self.description)  

    class Meta:
        db_table = 'invoice_line_item'

