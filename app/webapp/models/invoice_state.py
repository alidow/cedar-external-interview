
from django.db import models

class InvoiceState(models.Model):
    state = models.CharField(max_length=255)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    STATES = {
        'PENDING'					: 1,
        'COMPLETE'	                : 2,
        'READY'		                : 3,
        'PARTIAL'		            : 4,
        'PLAN'	                    : 5
    }

    def __str__(self):
        return self.state

    class Meta:
        db_table = 'invoice_state'
