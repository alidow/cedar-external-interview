from django.contrib.auth.models import BaseUserManager
from django.utils import timezone

class UserManager(BaseUserManager):

	def _create_user(self, username, email, password, **extra_fields):
		now = timezone.now()
		email = self.normalize_email(email)

		if username is None:
			user = self.model(email=email,last_name=extra_fields.pop("last_name"),
				first_name=extra_fields.pop("first_name"),
				is_superuser=extra_fields.pop("is_superuser"),
				is_staff=extra_fields.pop("is_staff")
			)

		else:
			username = self.model.normalize_username(username)
			user = self.model(username=username, email=email, **extra_fields)

		user.set_password(password)
		user.save(using=self._db)

		return user

	def create_user(self, username=None, email=None, password=None, **extra_fields):
		extra_fields.setdefault('is_staff', False)
		extra_fields.setdefault('is_superuser', False)

		return self._create_user(username, email, password, **extra_fields)

	def create_superuser(self, username, email, password, **extra_fields):
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_superuser', True)

		if extra_fields.get('is_staff') is not True:
			raise ValueError('Superuser must have is_staff=True.')
		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')

		return self._create_user(username, email, password, **extra_fields)




   