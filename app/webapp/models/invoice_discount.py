
from django.db import models

class InvoiceDiscount(models.Model):
    patient = models.ForeignKey('Patient', on_delete=models.CASCADE)
    invoice = models.ForeignKey('Invoice', on_delete=models.CASCADE)
    description = models.CharField(max_length=255, blank=True)
    # discount_percentage format: 10% -> 0.10
    # TODO: add check to make sure discount_percentage is never negative or > 1.0
    discount_percentage = models.DecimalField(decimal_places=4, max_digits=8)
    discount_start_date = models.DateTimeField(blank=True, null=True)
    discount_end_date = models.DateTimeField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.invoice, self.discount_percentage)  

    class Meta:
        db_table = 'invoice_discount'
