
from django.db import models
from django.core.validators import RegexValidator

class Provider(models.Model):
    organization = models.CharField(max_length=255)

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Value must be a valid phone number.")
    phone_primary = models.CharField(validators=[phone_regex], max_length=16)
    phone_secondary = models.CharField(validators=[phone_regex], max_length=16, blank=True)
    
    email = models.CharField(max_length=255)
    address = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=2, blank=True)
    zipcode = models.CharField(max_length=10, blank=True)
    country = models.CharField(max_length=3, blank=True)
    logo = models.CharField(max_length=300, blank=True)

    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.organization   

    class Meta:
        db_table = 'provider'

