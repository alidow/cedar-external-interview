
from django.db import models
from .patient import Patient
from django.core.validators import RegexValidator
from django.db.models import Q

class PatientInfo(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, blank=True, null=True)
    last_name = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    dob = models.DateField()

    ssn_regex = RegexValidator(regex=r'^\d{9}$', message="Value must be a valid social security number.")
    ssn = models.CharField(validators=[ssn_regex], max_length=9, blank=True)

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Value must be a valid phone number.")
    phone_cell = models.CharField(validators=[phone_regex], max_length=16)
    phone_home = models.CharField(validators=[phone_regex], max_length=16, blank=True)

    email = models.EmailField()
    address = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=2, blank=True)
    zipcode = models.CharField(max_length=10, blank=True)
    country = models.CharField(max_length=3, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)    

    class Meta:
        db_table = 'patient_info'

    def is_new(self):
        if self.pk is None:
            return True
        return False

    def save(self, *args, **kwargs):
        is_new_object = self.is_new()
        super(PatientInfo, self).save(*args, **kwargs)
        if is_new_object:
            from ..workers.tasks import on_create_patient_info
            on_create_patient_info(object.id)

    def match_with_patient(self):
        patient = Patient.objects.get(
                Q(first_name    =   self.first_name),
                Q(last_name     =   self.last_name),
                Q(dob           =   self.dob),
                Q(ssn           =   self.ssn)
            )
        if patient is None:
            return None
        return patient.update_contact_info(self)

    def update_or_create_patient(self):
        patient = self.match_with_patient()
        if patient is not None:
            self.patient = patient
            self.save()
            return patient;
        patient = Patient.objects.create(
                last_name       =   self.last_name,
                first_name      =   self.first_name,
                dob             =   self.dob,
                ssn             =   self.ssn,
                phone_cell      =   self.phone_cell,
                email           =   self.email,
                address         =   self.address,
                city            =   self.city,
                state           =   self.state,
                zipcode         =   self.zipcode,
                country         =   self.country
            )
        return patient