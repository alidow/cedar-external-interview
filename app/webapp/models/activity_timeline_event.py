
from datetime import datetime
from django.utils import timezone
from django.db import models
from .activity_timeline_event_type import ActivityTimelineEventType

class ActivityTimelineEvent(models.Model):

    STATUS_COMPLETE         = 1
    STATUS_WARNING          = 2
    STATUS_PAST_DUE         = 3

    patient = models.ForeignKey('Patient', on_delete=models.CASCADE)
    provider = models.ForeignKey('Provider', on_delete=models.CASCADE)
    visit = models.ForeignKey('Visit', on_delete=models.CASCADE)
    invoice = models.ForeignKey('Invoice', on_delete=models.CASCADE)
    insurer = models.ForeignKey('Insurer', on_delete=models.CASCADE)
    
    event_type = models.ForeignKey(ActivityTimelineEventType, blank=False, null=False)
    date = models.DateTimeField(blank=False, null=False)

    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    @property
    def status(self):
        if self.date <= timezone.now():
            # TODO: past due!
            return self.STATUS_COMPLETE
        else:
            return self.STATUS_WARNING

    def due_date_str(self):
        return "Bill due {:%Y-%m-%d}".format(self.date)

    def __str__(self):
        AT = ActivityTimelineEventType()
        mapping = {
            AT.VISIT                        : (lambda s: "Visit to {}.".format(s.provider.organization)),
            AT.INSURANCE_DONE_PROCESSING    : (lambda s: "{} finished processing bill.".format(s.insurer.name)),
            AT.BILL_SENT_VIA_MAIL           : (lambda s: "Bill sent via mail."),
            AT.BILL_SENT_VIA_EMAIL          : (lambda s: "Bill sent via email."),
            AT.BILL_SENT_VIA_TEXT_MSG       : (lambda s: "Bill sent via text message."),
            AT.BILL_DUE_DATE                : (lambda s: s.due_date_str()),
            AT.COLLECTIONS_DEADLINE         : (lambda s: "Pay before your credit score is damaged."),
            AT.FULL_PAYMENT_MADE            : (lambda s: "Bill fully paid. Thank you!"),
            AT.PARTIAL_PAYMENT_MADE         : (lambda s: "Partial payment."),
            AT.PAYMENT_PLAN_SETUP           : (lambda s: "Payment plan setup."),
            AT.PAYMENT_PLAN_PAYMENT         : (lambda s: "Payment plan scheduled payment made.")
        }
        if self.event_type is None:
            return "unknown"
        type_id = self.event_type.type_id
        if type_id not in mapping:
            return "unknown"
        else:
            return mapping[type_id](self)

    class Meta:
        db_table = 'activity_timeline_event'
