
from django.db import models
from django.core.validators import RegexValidator

class Insurer(models.Model):
    name = models.CharField(max_length=255)
    
    logo = models.URLField(blank=True)

    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name   

    class Meta:
        db_table = 'insurer'

