
from django.db import models

class Visit(models.Model):
    patient = models.ForeignKey('Patient', on_delete=models.CASCADE)
    provider = models.ForeignKey('Provider', on_delete=models.CASCADE)
    date_of_service = models.DateTimeField()
    description = models.CharField(max_length=255, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s" % (self.patient, self.date_of_service)

    class Meta:
        ordering = ['date_of_service']
        db_table = 'visit'