from datetime import datetime
from django.utils import timezone
from django.utils.functional import cached_property
from django.db import models
from .invoice_state import InvoiceState
from .invoice_line_item import InvoiceLineItem
from .activity_timeline_event import ActivityTimelineEvent
from .invoice_discount import InvoiceDiscount
from datetime import datetime
from decimal import *
from django.conf import settings
from django.db.models import Q
import pytz

class Invoice(models.Model):
    patient = models.ForeignKey('Patient', on_delete=models.CASCADE)
    provider = models.ForeignKey('Provider', on_delete=models.CASCADE)
    visit = models.ForeignKey('Visit', on_delete=models.CASCADE, blank=True, null=True)
    primary_insurer = models.ForeignKey('Insurer', blank=True, null=True)
    description = models.CharField(max_length=255, blank=True)
    doctor = models.CharField(max_length=255, blank=True)
    date_issued = models.DateTimeField()
    date_due = models.DateTimeField(blank=True, null=True)
    total_charge_amount = models.DecimalField(max_digits=12, decimal_places=2)
    paid = models.BooleanField()
    invoice_state = models.ForeignKey(InvoiceState, on_delete=models.CASCADE, blank=True,
        null=True, related_name='%(class)s_invoice_state')
    invoice_review_state = models.ForeignKey(InvoiceState, on_delete=models.CASCADE, blank=True, 
        null=True, related_name='%(class)s_invoice_review_state')
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.patient, self.description)  

    class Meta:
        db_table = 'invoice'

    @cached_property
    def activity_events(self):
        return self.activitytimelineevent_set.all().order_by('date')

    @cached_property
    def most_current_event(self):
        events = self.activity_events.order_by('date')
        now = timezone.now()
        last_event = None
        for event in events:
            if event.date <= now:
                last_event = event
        return last_event

    @cached_property
    def line_items(self):
        return self.invoicelineitem_set.all()

    @property
    def pre_insurance_total(self):
        amount = 0
        for line_item in self.line_items:
            amount += line_item.charge_amount
        return amount

    @property
    def is_new(self):
        if self.pk is None:
            return True
        return False

    @property
    def due_in_days(self):
        return (self.due_date.replace(tzinfo=None) -
                  datetime.datetime.now().replace(tzinfo=None)).days

    @cached_property
    def eligible_discounts(self):
        now = datetime.now(pytz.timezone(settings.TIME_ZONE))
        return InvoiceDiscount.objects.filter(
                    Q(invoice_id                = self.id), 
                    Q(discount_start_date__lt   = now) | Q(discount_start_date__isnull   = True), 
                    Q(discount_end_date__gt     = now) | Q(discount_end_date__isnull     = True), 
                    Q(deleted                   = False)
                )

    @property
    def best_discount(self):
        best_discount = None

        # For the time being, discounts only applied on initial payment. No discount if payments already made
        if self.amount_paid > 0:
            return best_discount

        for discount in self.eligible_discounts:
            if best_discount is None:
                best_discount = discount
            elif best_discount.discount_percentage < discount.discount_percentage:
                best_discount = discount
            elif (
                    best_discount.discount_percentage < discount.discount_percentage and
                    best_discount.discount_end_date < discount.end_date
                ):
                    best_discount = discount
        return best_discount

    @property
    def discount_amount(self):
        best_discount = self.best_discount
        if best_discount is None:
            return 0
        else:
            return round(self.remaining_charge_amount * best_discount.discount_percentage, 2)

    @property
    def discounted_amount(self):
        return self.remaining_charge_amount - self.discount_amount

    @cached_property
    def payments(self):
        return self.payment_set.all()

    @property
    def amount_paid(self):
        total = Decimal(0)
        for payment in self.payments:
            payment_discount = 0
            if payment.discount_amount:
                payment_discount = payment.discount_amount
            total += Decimal(payment.amount) + Decimal(payment_discount)
        return total

    @property
    def remaining_charge_amount(self):
        return self.total_charge_amount - self.amount_paid

    def save(self, *args, **kwargs):
        is_ready_to_send = False
        is_new_object = self.is_new

        if not is_new_object and self.invoice_review_state is not None:
            old_invoice = Invoice.objects.get(pk=self.pk)
            old_invoice_review_state = old_invoice.invoice_review_state

            if old_invoice_review_state is not None:
                is_ready_to_send = (old_invoice_review_state.state=='pending' and self.invoice_review_state.state=='complete')

        super(Invoice, self).save(*args, **kwargs)

        if is_ready_to_send:
            from ..workers.tasks import on_invoice_ready_to_send
            on_invoice_ready_to_send(self.id, self.patient.id)

    @staticmethod
    def get_outstanding_invoices_for_patient(patient_id):
        return Invoice.objects.filter(
                    patient_id  = patient_id, 
                    paid        = False, 
                    deleted     = False).order_by('-created_on')