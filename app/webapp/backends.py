from django.conf import settings
from .models import Patient
from datetime import datetime
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model
User = get_user_model()

class PatientAuthBackend(object):

    def authenticate(self, user_id=None, form_dob=None, form_ssn=None):
        user = self.get_user(user_id)
        if user is None or not user.is_active:
            return None

        patient_queryset = Patient.objects.filter(user=user)
        if not patient_queryset.exists():
            return None

        patient = patient_queryset[0]
        last_four_digits = patient.ssn[-4:]

        if form_dob == patient.dob and form_ssn == last_four_digits:
            return user

        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

class ModelBackendById(ModelBackend):

    def authenticate(self, username=None, password=None, **kwargs):
        # django converts None string values from db into empty strings so check both
        if username is None or not username:
            return None
        # super(ModelBackendById, self).authenticate(request=request, username=username, password=password, **kwargs)
        UserModel = get_user_model()

        try:
            user = UserModel._default_manager.get_by_natural_key(username)
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            UserModel().set_password(password)
        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                return user



