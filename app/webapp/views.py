from django.contrib.auth import authenticate, login as auth_login, logout
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render
from django.conf import settings
from dateutil import parser

from .models import *


def auth_with_test_user(func):
    def func_wrapper(*args, **kwargs):
        request = args[0]
        logout(request)
        patient = Patient.objects.all()[0]
        user = authenticate(user_id=patient.user.id,
                            form_dob=patient.dob, form_ssn=patient.last_4_ssn)
        auth_login(request, user)
        return func(*args)
    return func_wrapper


@auth_with_test_user
def index(request):
    return HttpResponse("Hello, world.")


@auth_with_test_user
def oops(request):
    return HttpRespons("Oops!")
